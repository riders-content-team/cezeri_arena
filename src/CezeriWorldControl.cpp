#include "CezeriWorldControl.hpp"

using namespace gazebo;
GZ_REGISTER_WORLD_PLUGIN(CezeriWorldControl)

CezeriWorldControl::CezeriWorldControl() : WorldPlugin()
{
    this->models_spawned = 0;

}

CezeriWorldControl::~CezeriWorldControl()
{
    
}

void CezeriWorldControl::Load(physics::WorldPtr _world, sdf::ElementPtr _sdf)
{
    this->world = _world;

    // Connect to the world update event.
    this->worldConnection = event::Events::ConnectWorldUpdateBegin(
            std::bind(&CezeriWorldControl::Update, this));

    // Connect to the world reset event.
    this->resetConnection = event::Events::ConnectWorldReset(
            std::bind(&CezeriWorldControl::Reset, this));

    this->InitRosStuff();


}

void CezeriWorldControl::Update()
{
    if(ros::ok())
    {
        ros::spinOnce();
    }
}


void CezeriWorldControl::Reset()
{

    for(std::vector<std::string>::iterator it = std::begin(this->spawned_model_names); 
            it != std::end(this->spawned_model_names); ++it) 
    {
        this->world->RemoveModel(*it);
    }
    this->spawned_model_names.clear();
    this->models_spawned = 0;
}


void CezeriWorldControl::InitRosStuff()
{
    if(!ros::isInitialized())
    {
        int argc = 0;
        char **argv = NULL;
        ros::init(
            argc,
            argv,
            this->node_name,
            ros::init_options::NoSigintHandler
        );
    }

    this->rosNode.reset(new ros::NodeHandle(this->node_name));

    ros::AdvertiseServiceOptions spawn_weather_options = 
        ros::AdvertiseServiceOptions::create<cezeri_arena::SpawnWeather>
        (
            "/spawn_weather",
            boost::bind(&CezeriWorldControl::SpawnWeatherCallback, this, _1, _2),
            ros::VoidConstPtr(),
            NULL
        );

    this->spawn_weather_service = this->rosNode->advertiseService(spawn_weather_options);
    
    ros::AdvertiseServiceOptions add_traffic_options = 
        ros::AdvertiseServiceOptions::create<cezeri_arena::AddTraffic>
        (
            "/add_traffic",
            boost::bind(&CezeriWorldControl::AddTrafficCallback, this, _1, _2),
            ros::VoidConstPtr(),
            NULL
        );

    this->add_traffic_service = this->rosNode->advertiseService(add_traffic_options);
    
    ros::AdvertiseServiceOptions spawn_model_options = 
        ros::AdvertiseServiceOptions::create<cezeri_arena::SpawnModel>
        (
            "/spawn_model",
            boost::bind(&CezeriWorldControl::SpawnModelCallback, this, _1, _2),
            ros::VoidConstPtr(),
            NULL
        );

    this->spawn_model_service = this->rosNode->advertiseService(spawn_model_options);
}


bool CezeriWorldControl::SpawnModelCallback(cezeri_arena::SpawnModel::Request &request,
                                              cezeri_arena::SpawnModel::Response &response)
{
      
    response.spawn_name = this->SpawnSDFModel(request.model_name, request.x, request.y, 0);
    return true;
}


bool CezeriWorldControl::SpawnWeatherCallback(cezeri_arena::SpawnWeather::Request &request,
                                              cezeri_arena::SpawnWeather::Response &response)
{
      
    response.spawn_name = this->SpawnSDFModel(request.model_name, request.x, request.y, 0);
    return true;
}

bool CezeriWorldControl::AddTrafficCallback(cezeri_arena::AddTraffic::Request &request,
                                            cezeri_arena::AddTraffic::Response &response)
{
    std::string model_name = "cezeri"; 
    sdf::SDFPtr model = this->GetModelSDF(model_name);
    sdf::ElementPtr plugin_elem = model->Root()->GetElement("model")->GetElement("plugin");

    std::string str_x = std::to_string(request.start_x);
    std::string str_y = std::to_string(request.start_y);
    std::string str_z = std::to_string(request.start_z);
    std::string pose_values = str_x + " " + str_y + " " + str_z + " 0 0 0";

    std::string spawn_model_name = model_name + "_" + std::to_string(this->models_spawned);
    
    model->Root()->GetElement("model")->GetAttribute("name")->SetFromString(spawn_model_name);
    model->Root()->GetElement("model")->AddElement("pose")->AddValue("pose", pose_values, true);
    
    plugin_elem->GetElement("destinationX")->AddValue("string", std::to_string(request.dest_x), true);
    plugin_elem->GetElement("destinationY")->AddValue("string", std::to_string(request.dest_y), true);
    //plugin_elem->GetElement("altitudeMin")->AddValue("string", std::to_string(request.altitude_min), true);
    //plugin_elem->GetElement("altitudeMax")->AddValue("string", std::to_string(request.altitude_max), true);
    plugin_elem->GetElement("speed")->AddValue("string", std::to_string(request.speed), true);

    /* std::cout << plugin_elem->ToString("") << std::endl; */

    this->world->InsertModelSDF(*model);
    
    response.spawn_name = spawn_model_name;
    this->spawned_model_names.push_back(spawn_model_name);
    this->models_spawned++;
    return true;
}

std::string CezeriWorldControl::SpawnSDFModel(std::string model_name, double pos_x, double pos_y, double pos_z)
{
    sdf::SDFPtr model = this->GetModelSDF(model_name);

    std::string str_x = std::to_string(pos_x);
    std::string str_y = std::to_string(pos_y);
    std::string str_z = std::to_string(pos_z);
    std::string pose_values = str_x + " " + str_y + " " + str_z + " 0 0 0";

    std::string spawn_model_name = model_name + "_" + std::to_string(this->models_spawned);
    
    model->Root()->GetElement("model")->GetAttribute("name")->SetFromString(spawn_model_name);
    model->Root()->GetElement("model")->AddElement("pose")->AddValue("pose", pose_values, true);

    this->world->InsertModelSDF(*model);
    
    this->spawned_model_names.push_back(spawn_model_name);
    this->models_spawned++;

    return spawn_model_name;
}


sdf::SDFPtr CezeriWorldControl::GetModelSDF(std::string model_name)
{
    sdf::SDFPtr model(new sdf::SDF());
    sdf::init(model);
    sdf::readFile(this->GetModelPath(model_name) + "/model.sdf", model);    
    
    return model;
}


std::string CezeriWorldControl::GetModelPath(std::string model_name)
{
    struct dirent *entry;
    DIR *dir;

    std::string gazebo_model_path = std::getenv("GAZEBO_MODEL_PATH");
    std::string delimiter = ":";
    std::vector<std::string> model_paths;

    size_t pos = 0;
    std::string token;

    while ((pos = gazebo_model_path.find(delimiter)) != std::string::npos)
    {
        token = gazebo_model_path.substr(0, pos);
        model_paths.push_back(token);
        gazebo_model_path.erase(0, pos + delimiter.length());
    }
    model_paths.push_back(gazebo_model_path);

    sort(model_paths.begin(), model_paths.end());
    model_paths.erase(unique(model_paths.begin(), model_paths.end()), model_paths.end());

    for (std::vector<std::string>::iterator mp = model_paths.begin();
      mp != model_paths.end(); ++mp) {
        if (dir = opendir((*mp).c_str()))
        {
            while (entry = readdir(dir))
            {
                if (!model_name.compare(entry->d_name))
                {
                    closedir(dir);
                    return *mp + "/" + model_name;
                }
            }
            closedir(dir);
        }
    }
    return NULL;
}
