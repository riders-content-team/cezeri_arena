#include "WindAnimation.hpp"

using namespace gazebo;
// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(WindAnimation)

// Constructor
WindAnimation::WindAnimation() : ModelPlugin()
{
    this->gen = std::mt19937(this->rd());
    this->state = NONE;
}

// Destructor
WindAnimation::~WindAnimation()
{
}

void WindAnimation::Load(physics::ModelPtr model, sdf::ElementPtr _sdf)
{
    this->model = model;
    this->sdf = _sdf;
    this->world = this->model->GetWorld();
    this->robot_name = "ringrider";
    
    this->InitSDFParams();

    ignition::math::Pose3d start_pos = this->model->WorldPose();
    this->start_x = start_pos.Pos().X();
    this->start_y = start_pos.Pos().Y();
    
    this->dis = std::uniform_real_distribution<>(this->altitude_min, this->altitude_max);
    
    this->target_altitude = this->dis(this->gen);
    this->state = SETTING_HEIGHT;
    
    if(this->update_rate > 0.0) {
        this->update_period = 1.0 / this->update_rate;
    }
    else
    {
        this->update_period = 0.0;
    }

    this->last_update_time = this->world->SimTime().Double();


    double dx = this->dest_x - this->start_x;
    double dy = this->dest_y - this->start_y;
    double distance = std::sqrt(std::pow(dx, 2) + std::pow(dy,2));

    double v_x = (this->speed * (dx / distance)) / this->update_rate;
    double v_y = (this->speed * (dy / distance)) / this->update_rate;

    this->v_pose = ignition::math::Pose3d(
        v_x,
        v_y,
        0,

        0,
        0,
        0
    );

    this->a_pose = ignition::math::Pose3d(0, 0, this->speed / this->update_rate, 0, 0, 0);
    
    this->worldConnection = event::Events::ConnectWorldUpdateBegin(
        std::bind(&WindAnimation::Update, this));
}

void WindAnimation::Update()
{
    double current_time = this->world->SimTime().Double();
    double seconds_since_last_update = current_time - this->last_update_time;

    if (seconds_since_last_update > this->update_period)
    {
        //this->joints[LEFT_FRONT]->SetParam("vel", 0, this->rotation_speed);
        //this->joints[RIGHT_FRONT]->SetParam("vel", 0, this->rotation_speed);
        //this->joints[LEFT_REAR]->SetParam("vel", 0, this->rotation_speed);
        //this->joints[RIGHT_REAR]->SetParam("vel", 0, this->rotation_speed);

        ignition::math::Pose3d pose = this->model->WorldPose();
        
        if(this->state == SETTING_HEIGHT)
        {
            if(pose.Pos().Z() > this->target_altitude)
            {
                pose -= this->a_pose;
            }
            else
            {
                pose += this->a_pose;
            }

            if(std::abs(pose.Pos().Z() - this->target_altitude) < this->a_pose.Pos().Z())
            {
                double dx;
                double dy;

                dx = this->start_x - pose.Pos().X();
                dy = this->start_y - pose.Pos().Y();
                double distance_to_start = std::sqrt(std::pow(dx, 2) + std::pow(dy,2));
            
                dx = this->dest_x - pose.Pos().X();
                dy = this->dest_y - pose.Pos().Y();
                double distance_to_dest = std::sqrt(std::pow(dx, 2) + std::pow(dy,2));

                if((distance_to_start - distance_to_dest) > 0)
                {
                    this->state = TO_START;
                }
                else
                {
                    this->state = TO_DEST;
                }
            }
        }
        else if(this->state == TO_DEST)
        {
            pose += this->v_pose;

            double dx = this->dest_x - pose.Pos().X();
            double dy = this->dest_y - pose.Pos().Y();

            double vx = this->v_pose.Pos().X();
            double vy = this->v_pose.Pos().Y();

            if(std::abs(dx) < std::abs(vx) && std::abs(dy) < std::abs(vy))
            {
                this->target_altitude = this->dis(this->gen);
                this->state = SETTING_HEIGHT;
            }
        }
        else if(this->state == TO_START)
        {
            pose -= this->v_pose;

            double dx = this->start_x - pose.Pos().X();
            double dy = this->start_y - pose.Pos().Y();

            double vx = this->v_pose.Pos().X();
            double vy = this->v_pose.Pos().Y();

            if(std::abs(dx) < std::abs(vx) && std::abs(dy) < std::abs(vy))
            {
                this->target_altitude = this->dis(this->gen);
                this->state = SETTING_HEIGHT;
            }
        }

        this->model->SetWorldPose(pose);

        this->last_update_time = this->world->SimTime().Double();
    }
}