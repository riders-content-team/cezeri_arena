#ifndef CEZERI_ANIMATION_HH
#define CEZERI_ANIMATION_HH

#include "gazebo/common/common.hh"
#include "gazebo/physics/PhysicsTypes.hh"
#include "gazebo/physics/MultiRayShape.hh"
#include "gazebo/physics/World.hh"
#include "gazebo/physics/Model.hh"
#include "gazebo/physics/Joint.hh"
#include "gazebo/physics/PhysicsIface.hh"
#include <ignition/math/Pose3.hh>
#include <random>

namespace gazebo
{
    class GAZEBO_VISIBLE CezeriAnimation : public ModelPlugin
    {
        public: 
            CezeriAnimation();
            ~CezeriAnimation();
            virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf);
            virtual void Update();

        private:

            void InitSDFParams();

            physics::ModelPtr model;
            physics::WorldPtr world;
            sdf::ElementPtr sdf;

            event::ConnectionPtr worldConnection;

            std::string left_front_joint_name, right_front_joint_name,
                left_rear_joint_name, right_rear_joint_name, robot_name;

            double update_period, last_update_time, rotation_speed, update_rate;

            enum wheels
            {
                RIGHT_FRONT = 0,
                LEFT_FRONT = 1,
                RIGHT_REAR = 2,
                LEFT_REAR = 3
            };

            enum states
            {
                NONE = 0,
                SETTING_HEIGHT = 1,
                TO_DEST = 2,
                TO_START = 3
            };

            double start_x, start_y, dest_x, dest_y, altitude_min, altitude_max, speed;

            physics::JointPtr joints[4];


            ignition::math::Pose3d v_pose;
            ignition::math::Pose3d a_pose;

            std::random_device rd;
            std::mt19937 gen;
            std::uniform_real_distribution<> dis;
            double target_altitude;

            uint8_t state;

    };
}
#endif
