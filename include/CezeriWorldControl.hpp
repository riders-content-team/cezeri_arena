#ifndef CEZERI_WORLD_CONTROL_HPP_
#define CEZERI_WORLD_CONTROL_HPP_

#include "gazebo/gazebo.hh"
#include "gazebo/common/Plugin.hh"
#include "gazebo/common/common.hh"
#include "gazebo/physics/physics.hh"
#include "sdf/sdf.hh"
#include "sdf/Element.hh"
#include <stdio.h>
#include <iostream>
#include <string.h>

#include <ros/ros.h>
#include "cezeri_arena/SpawnWeather.h"
#include "cezeri_arena/AddTraffic.h"
#include "cezeri_arena/SpawnModel.h"

namespace gazebo
{
	class GAZEBO_VISIBLE CezeriWorldControl : public WorldPlugin
	{
		public: 
            std::unique_ptr<ros::NodeHandle> rosNode;

            CezeriWorldControl();
            ~CezeriWorldControl();
            virtual void Load(physics::WorldPtr _world, sdf::ElementPtr _sdf);
            void InitRosStuff();
            void Update();
            void Reset();

            bool SpawnWeatherCallback(
                cezeri_arena::SpawnWeather::Request &request,
                cezeri_arena::SpawnWeather::Response &response
            );
        
            bool AddTrafficCallback(
                cezeri_arena::AddTraffic::Request &request,
                cezeri_arena::AddTraffic::Response &response
            );
            
            bool SpawnModelCallback(
                cezeri_arena::SpawnModel::Request &request,
                cezeri_arena::SpawnModel::Response &response
            );

        private:
            physics::WorldPtr world;
            std::string node_name = "cezeri_world_control";
            
            event::ConnectionPtr worldConnection, resetConnection;
            ros::ServiceServer spawn_weather_service, add_traffic_service, spawn_model_service;


            uint8_t models_spawned;
            std::vector<std::string> spawned_model_names;
            
            std::string SpawnSDFModel(std::string model_name, double pos_x, double pos_y, double pos_z);
            sdf::SDFPtr GetModelSDF(std::string model_name);
            std::string GetModelPath(std::string model_name); 
    };
}

#endif
